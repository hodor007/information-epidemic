import numpy as np
import networkx as nx
from girg import *
#import matplotlib.pyplot as plt

seed = 123

#########################################################################
#########################################################################

### structure ###

### edge_removal ###

def random_edge_removal(g, q, p):
    N = len(g)
    alls = list(range(N))
    n = 0
    while n < q*N:
        node = np.random.choice(alls)
        alls.remove(node)
        n += 1
        for neighbor in list(g.neighbors(node)):
            rand = np.random.random()
            if rand < p:
                g.remove_edge(node, neighbor)
    return g

def targeted_edge_removal(g, q, p):
    N = len(g)
    degree_node = list(nx.degree_centrality(g).values())
    n = 0
    while n < q*N:
        hub = np.argmax(degree_node)
        degree_node[hub] = -1
        n += 1
        for neighbor in list(g.neighbors(hub)):
            rand = np.random.random()
            if rand < p:
                g.remove_edge(hub, neighbor)
    return g


### neighbors ###

def neighbors_of_nodes(g):
    N = len(g)
    node_neighbors = []
    for i in range(N):
        node_neighbors.append([n for n in g.neighbors(i)])
    return node_neighbors

#########################################################################

### dynamics ###

### initial_condition ###

def set_initial_condition(N0, first, second):
    n0 = 0
    while n0 < N0:
        ind = np.random.choice(first)
        first.remove(ind)
        second.append(ind)
        n0 += 1
    return first, second

def identify_distinct_neighbors_of_nodes(distinct, node_neighbors):
    N = len(node_neighbors)
    node_distinct_neighbors = []
    for i in range(N):
        node_distinct_neighbors.append(list(set(distinct).intersection(set(node_neighbors[i]))))
    return node_distinct_neighbors


### rates ###

### uau

def r_u_a():
    r_lambd_unawares = []
    repeated_unawares = []
    for unaware in unawares:
        number_of_aware_neighbors = len(il_node_aware_neighbors[unaware])
        r_lambd_unawares.append(lambd * number_of_aware_neighbors)
        repeated_unawares.extend([unaware]*number_of_aware_neighbors)
    r_lambd = sum(r_lambd_unawares)
    return r_lambd, repeated_unawares

def r_u_a_i():
    r_kappa = kappa * len(unawares_infecteds)
    repeated_unawares_infecteds = unawares_infecteds
    return r_kappa, repeated_unawares_infecteds

def r_a_u():
    r_delta = delta * len(awares)
    repeated_awares = awares
    return r_delta, repeated_awares
    

### seird

def r_s_e_u():
    r_beta_unawares_susceptibles = []
    repeated_unawares_susceptibles = []
    for unaware_susceptible in unawares_susceptibles:
        number_of_infected_neighbors = len(el_node_infected_neighbors[unaware_susceptible])
        r_beta_unawares_susceptibles.append(beta_u * number_of_infected_neighbors)
        repeated_unawares_susceptibles.extend([unaware_susceptible]*number_of_infected_neighbors)
    r_beta_u = sum(r_beta_unawares_susceptibles)
    return r_beta_u, repeated_unawares_susceptibles

def r_s_e_a():
    r_beta_awares_susceptibles = []
    repeated_awares_susceptibles = []
    for aware_susceptible in awares_susceptibles:
        number_of_infected_neighbors = len(el_node_infected_neighbors[aware_susceptible])
        r_beta_awares_susceptibles.append(beta_a * number_of_infected_neighbors)
        repeated_awares_susceptibles.extend([aware_susceptible]*number_of_infected_neighbors)
    r_beta_a = sum(r_beta_awares_susceptibles)
    return r_beta_a, repeated_awares_susceptibles

def r_e_i():
    r_sigma = sigma * len(exposeds)
    repeated_exposeds = exposeds
    return r_sigma, repeated_exposeds

def r_i_r_or_d():
    r_gamma = gamma*(1-f) * len(infecteds)
    r_f = gamma*f * len(infecteds)
    repeated_infecteds = infecteds
    return r_gamma, r_f, repeated_infecteds


### transitions ###

### uau

def u_a():
    random_unaware = np.random.choice(repeated_unawares)
    unawares.remove(random_unaware)
    awares.append(random_unaware)
    if random_unaware in unawares_infecteds:
        unawares_infecteds.remove(random_unaware)
    elif random_unaware in unawares_susceptibles:
        unawares_susceptibles.remove(random_unaware)
        awares_susceptibles.append(random_unaware)
        #unawares_not_infecteds.remove(random_unaware)
    #else:
        #unawares_not_infecteds.remove(random_unaware)
    for neighbor in il_node_neighbors[random_unaware]:
        il_node_aware_neighbors[neighbor].append(random_unaware)

def u_a_i():
    random_unaware_infected = np.random.choice(repeated_unawares_infecteds)
    unawares.remove(random_unaware_infected)
    awares.append(random_unaware_infected)
    unawares_infecteds.remove(random_unaware_infected)
    for neighbor in il_node_neighbors[random_unaware_infected]:
        il_node_aware_neighbors[neighbor].append(random_unaware_infected)

def a_u():
    random_aware = np.random.choice(repeated_awares)
    awares.remove(random_aware)
    unawares.append(random_aware)
    if random_aware in infecteds:
        unawares_infecteds.append(random_aware)
    elif random_aware in awares_susceptibles:
        awares_susceptibles.remove(random_aware)
        unawares_susceptibles.append(random_aware)
        #unawares_not_infecteds.append(random_aware)
    #else:
        #unawares_not_infecteds.append(random_aware)
    for neighbor in il_node_neighbors[random_aware]:
        il_node_aware_neighbors[neighbor].remove(random_aware)
   

### seird

def s_e_u():
    random_unaware_susceptible = np.random.choice(repeated_unawares_susceptibles)
    susceptibles.remove(random_unaware_susceptible)
    unawares_susceptibles.remove(random_unaware_susceptible)
    exposeds.append(random_unaware_susceptible)

def s_e_a():
    random_aware_susceptible = np.random.choice(repeated_awares_susceptibles)
    susceptibles.remove(random_aware_susceptible)
    awares_susceptibles.remove(random_aware_susceptible)
    exposeds.append(random_aware_susceptible)

def e_i():
    random_exposed = np.random.choice(repeated_exposeds)
    exposeds.remove(random_exposed)
    infecteds.append(random_exposed)
    if random_exposed in unawares:
        #unawares_not_infecteds.remove(random_exposed)
        unawares_infecteds.append(random_exposed)
    for neighbor in el_node_neighbors[random_exposed]:
        el_node_infected_neighbors[neighbor].append(random_exposed)

def i_r():
    random_infected = np.random.choice(repeated_infecteds)
    infecteds.remove(random_infected)
    recoverds.append(random_infected)
    if random_infected in unawares:
        unawares_infecteds.remove(random_infected)
        #unawares_not_infecteds.append(random_infected)
    for neighbor in el_node_neighbors[random_infected]:
        el_node_infected_neighbors[neighbor].remove(random_infected)

def i_d():
    random_infected = np.random.choice(repeated_infecteds)
    infecteds.remove(random_infected)
    deads.append(random_infected)
    #u
    if random_infected in unawares:
        #unawares.remove(random_infected)
        #unawares_infecteds.remove(random_infected)
        ##unawares_not_infecteds.append(random_infected)
        pass
    #a
    elif random_infected in awares:
        awares.remove(random_infected)
        unawares.append(random_infected)
    for neighbor in el_node_neighbors[random_infected]:
        el_node_infected_neighbors[neighbor].remove(random_infected)


#########################################################################
#########################################################################


### parameters

N_ = 10000 + 940   # system size (number of nodes in each layer) # 940 (2.5), 245 (3.5)

T = 200     # time steps

### il

m = 2    # connection number # 2 [in BA]

kappa = 1 # il : u(i) -> a
lambd = 0.5*kappa # il : u -> a
delta = 1/30 # forgetness : a -> u

### el

alpha = 2 # density^(-1) # 2 [in GIRG]
tau = 2.5 # longrangeness^(-1) # 2.5 (long range), 3.5 (short range)  [in GIRG]

beta_u = 0.17 # el : s(u) -> e # 0.2 (tau=2.5 : long range) , 0.6 (tau=3.5 : short range)
beta_a = 1*beta_u # el : s(a) -> e
sigma = 1/5 # el : e -> i
f = 0.01 # fatality
gamma = 1/14 # resolution


###########################################
###########################################

### structure ###

###########################################

### epidemic layer (el) : GIRG

np.random.seed(seed)
Girg = GIRG(vertex_size=N_, alpha=alpha, tau=tau, expected_weight=1, on_torus=False, dimension=2)
np.random.seed(seed)
el_, locations = Girg.generate_graph(locations=Girg.vertex_locations, weights=Girg.vertex_weights)

connected_components = sorted(list(el_.subgraph(c) for c in nx.connected_components(el_)), key=len, reverse=True)

el = connected_components[0]

am = nx.to_numpy_matrix(el)

el = nx.Graph(am)

N = len(el)

#print('density_el', nx.density(el), nx.is_connected(el))

###########################################

### information layer (il) : BA

il = nx.barabasi_albert_graph(N,m,seed=seed)

#print('density_il',nx.density(il), nx.is_connected(il))

###########################################

il_node_neighbors = neighbors_of_nodes(il)

el_node_neighbors = neighbors_of_nodes(el)

###########################################
###########################################


path = 'data/without_uau/'+str(tau)

e0 = 0 #int(np.loadtxt(path+'/'+'e0'))
ens = 10

for e in range(e0, ens):

    ### initial_condition

    # il : u,a

    A0 = 1
    U0 = N-A0

    unawares = list(range(N))
    awares = []

    unawares, awares = set_initial_condition(A0, unawares, awares)
    
    il_node_aware_neighbors = identify_distinct_neighbors_of_nodes(awares, il_node_neighbors)

    # el : s,e,i,r,d

    E0 = 0
    I0 = int(N/1000)
    R0 = 0
    D0 = 0
    S0 = N-E0-I0-R0-D0

    susceptibles = list(range(N))
    exposeds = []
    infecteds = []
    recoverds = []
    deads = []

    susceptibles, exposeds = set_initial_condition(E0, susceptibles, exposeds)
    susceptibles, infecteds = set_initial_condition(I0, susceptibles, infecteds)
    susceptibles, recoverds = set_initial_condition(R0, susceptibles, recoverds)
    susceptibles, deads = set_initial_condition(D0, susceptibles, deads)

    el_node_infected_neighbors = identify_distinct_neighbors_of_nodes(infecteds, el_node_neighbors)


    # unawares:

    unawares_infecteds = set(infecteds).intersection(set(unawares))
    #unawares_not_infecteds = set(unawares) - unawares_infecteds

    unawares_infecteds = list(unawares_infecteds)
    #unawares_not_infecteds = list(unawares_not_infecteds)


    # susceptibles:

    unawares_susceptibles = set(susceptibles).intersection(set(unawares))
    awares_susceptibles = set(susceptibles) - unawares_susceptibles

    unawares_susceptibles = list(unawares_susceptibles)
    awares_susceptibles = list(awares_susceptibles)



    ### dynamics ###

    # populations

    t = 0
    ts = [t]

    U_t = [U0]
    A_t = [A0]

    S_t = [S0]
    E_t = [E0]
    I_t = [I0]
    R_t = [R0]
    D_t = [D0]

    while t < T:
        
        ### uau ###
        
        # u -> a (lambd)
        r_lambd, repeated_unawares = r_u_a()
        
        # u(i) -> a (kappa)
        r_kappa, repeated_unawares_infecteds = r_u_a_i()
        
        # a -> u (delta)
        
        r_delta , repeated_awares = r_a_u()
        
        
        ### seird ###
        
        # s(u) -> e (beta_u)
        r_beta_u, repeated_unawares_susceptibles = r_s_e_u()
        
        # s(a) -> e (beta_a)
        r_beta_a, repeated_awares_susceptibles = r_s_e_a()
        
        # e -> i (sigma)
        r_sigma, repeated_exposeds = r_e_i()
        
        # i -> r/d (gamma,f)
        r_gamma, r_f, repeated_infecteds = r_i_r_or_d()
        
        ###########################
        
        Q = r_lambd + r_kappa + r_delta + r_beta_u + r_beta_a + r_sigma + r_gamma + r_f
        
        if Q == 0:
            break
        
        rand = np.random.random()
        
        ### uau ###
        
        if rand < r_lambd/Q:
            # u -> a
            u_a()            
        
        elif r_lambd/Q < rand < (r_lambd+r_kappa)/Q:
            # u(i) -> a
            u_a_i()
        
        elif (r_lambd+r_kappa)/Q < rand < (r_lambd+r_kappa+r_delta)/Q:
            # a -> u
            a_u()
                
        ### seird ###
        
        elif (r_lambd+r_kappa+r_delta)/Q < rand < (r_lambd+r_kappa+r_delta+r_beta_u)/Q:
            # s(u) -> e
            s_e_u()
        
        elif (r_lambd+r_kappa+r_delta+r_beta_u)/Q < rand < (r_lambd+r_kappa+r_delta+r_beta_u+r_beta_a)/Q:
            # s(a) -> e
            s_e_a()
        
        elif (r_lambd+r_kappa+r_delta+r_beta_u+r_beta_a)/Q < rand < (r_lambd+r_kappa+r_delta+r_beta_u+r_beta_a+r_sigma)/Q:
            # e -> i
            e_i()
        
        elif (r_lambd+r_kappa+r_delta+r_beta_u+r_beta_a+r_sigma)/Q < rand < (r_lambd+r_kappa+r_delta+r_beta_u+r_beta_a+r_sigma+r_gamma)/Q:
            # i -> r
            i_r()
        
        elif (r_lambd+r_kappa+r_delta+r_beta_u+r_beta_a+r_sigma+r_gamma)/Q < rand < (r_lambd+r_kappa+r_delta+r_beta_u+r_beta_a+r_sigma+r_gamma+r_f)/Q:
            # i -> d
            i_d()
        
        ################################################################
        
        dt = -np.log(np.random.random())/Q
        t += dt
        
        ts.append(t)
        
        U_t.append(len(unawares))
        A_t.append(len(awares))
        S_t.append(len(susceptibles))
        E_t.append(len(exposeds))
        I_t.append(len(infecteds))
        R_t.append(len(recoverds))
        D_t.append(len(deads))
    
    np.save(path+'/'+'ts_'+str(e), ts)
    
    np.save(path+'/'+'U_t_'+str(e), U_t)
    np.save(path+'/'+'A_t_'+str(e), A_t)
    np.save(path+'/'+'S_t_'+str(e), S_t)
    np.save(path+'/'+'E_t_'+str(e), E_t)
    np.save(path+'/'+'I_t_'+str(e), I_t)
    np.save(path+'/'+'R_t_'+str(e), R_t)
    np.save(path+'/'+'D_t_'+str(e), D_t)
    
    last_S = S_t[-1]
    height_I = max(I_t)
    time_I = ts[np.argmax(I_t)]
    
    np.save(path+'/'+'S_'+str(e), last_S)
    np.save(path+'/'+'I_'+str(e), height_I)
    np.save(path+'/'+'t_'+str(e), time_I)
    
    '''
    plt.figure(figsize=(15,10))
    plt.plot(ts, U_t, label='U')
    plt.plot(ts, A_t, label='A')
    plt.plot(ts, S_t, label='S')
    plt.plot(ts, E_t, label='E')
    plt.plot(ts, I_t, label='I')
    plt.plot(ts, R_t, label='R')
    plt.plot(ts, D_t, label='D')
    plt.legend()
    plt.plot(time_I, height_I, marker='o', markersize=5, color='black')
    plt.plot([0,T], [last_S, last_S], color='black')
    plt.show()
    '''
    
    #e0 += 1
    #np.savetxt(path+'/'+'e0', [e0])
    
    print(e)
