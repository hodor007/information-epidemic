# Information Epidemic

This project provides implementations of multilayer network models of coupled susceptible-exposed-infected-recovered-deceased (SEIRD) and unaware-aware-unaware (UAU) dynamics.

<div align="center">
<img width="800" src="schematic.png" alt="SEIRD-UAU model">
</div>

The above figure shows a schematic of our model. (a) Information layer and epidemic layer. Nodes in the information layer are either unaware ($U$) or aware ($A$) while nodes in the epidemic layer can be in one of five different states: susceptible ($S$), exposed ($E$), infected ($I$), recovered ($R$), and deceased ($D$). Edge removal that is caused by disruptions in the information layer is indicated by the scissor symbol. (b) Unaware nodes become aware at rate $\lambda$ if they are adjacent to an aware node. If unaware nodes are infected, they can also become aware at rate $\kappa$. Aware nodes transition back to an unaware state at rate $\delta$. (c) Infectious nodes transmit a disease to unaware and aware susceptible nodes at rates $\beta^{\mathrm{u}}$ and $\beta^{\mathrm{a}}$, respectively. To account for a reduction in infectiousness risk of aware nodes, we assume the value of the disease transmission rate $\beta^{\mathrm{u}}$ associated with unaware nodes is strictly larger than the value of the disease transmission rate $\beta^{\mathrm{a}}$ associated with aware nodes ($\beta^{\mathrm{u}} \ge \beta^{\mathrm{a}}$). Once susceptible nodes have been infected, they enter an exposed state and become infectious at rate $\sigma$. The characteristic time scale $\sigma^{-1}$ corresponds to the latency period of the disease. Infected nodes either die or recover at rates $f\gamma$ and $(1-f)\gamma$, respectively.

Some of our implementations use the [parmap](https://github.com/zeehio/parmap) python module to parallelize mean-field calculations.

## Reference
* H. Masoomy, T. Chou, L. Böttcher, Impact of random and targeted disruptions on information diffusion during outbreaks, preprint

Please cite our work if you find the above implementations helpful for your own research projects.

```
@article{masoomyimpact2023,
  title={Impact of random and targeted disruptions on information diffusion during outbreaks},
  author={Masoomy, Hosein and Chou, Tom and B{\"o}ttcher, Lucas},
  year={2023}
}
```
